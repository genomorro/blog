# Blog personal

Blog hecho ahora con Org-mode. Espero de esta manera unificar mi escritura en Emacs y así tener el mismo archivo para el blog que para textos que debo entregar, no trabajar doble.

El blog se genera con gitlab, uso la [configuración de manual](https://gitlab.com/pages/org-mode/-/blob/master/.gitlab-ci.yml), ningún cambio.

## Errores conocidos

- Al exportar las fechas, éstas aparecen en inglés y no en español. Debe ser un error porque no se configura el contenedor en el que se genera el sitio.
- Los enlaces tienen el mismo aspecto que el texto estándar, es difícil saber donde hay un enlace. Seguro base.css requiere un `!important` y listo. Elegir un color adecuado para el enlace es otra cosa.
- Los `#+KEYWORDS` declarados en los post deberían funcionar como etiquetas para el sitio. Aunque aparecen en el index no tienen función.
- Este archivo sigue aún en MD.
- El tamaño del frame de yt debe ser mejor.
