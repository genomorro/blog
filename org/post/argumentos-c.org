#+SETUPFILE: ../../templates/setup.org
#+SETUPFILE: ../../templates/level-1.org
#+TITLE: Uso de argumentos en C
#+SUBTITLE: Publicado: {{{date}}}
#+DESCRIPTION: Cómo funcionan los argumentos de un programa en C
#+DATE: <2021-01-14>
#+KEYWORDS: C programación

Esto es una breve explicación de como funcionan los argumentos de un programa en lenguaje C. Lo primero que hay que decir es que hace años no programo en C, sin embargo hay quien tiene esta inquietud, por lo cual es necesario desempolvar la memoria. Siempre será mejor el estándar actual de C o un libro actualizado.

** Principios básicos
Primero, un programa sencillo en C debe tener el siguiente esqueleto:

#+begin_src c
  #include <stdio.h>
  
  int main(int argc, char *argv[]){
    return 0;
  }
#+end_src
    
Note que la función main regresará un valor 0 si se ejecuta correctamente, además lo importante son los parámetros que recibe la función. ~argc~ es el número de argumentos que hay en la línea de comandos (un contador), mientras que ~argv~ es un arreglo que contiene cada uno de esos argumentos en forma de cadena de caracteres.
    
En el lenguaje C el primer argumento será siempre el nombre del programa, y se almacenará en ~argv[0]~, los argumentos se guardan secuencialmente. El siguiente ejemplo ilustra esta situación:

#+begin_src c
  #include <stdio.h>
  
  int main(int argc, char *argv[])
  {
    int i;
    printf("El número de argumentos. %i \n", argc);
    printf("El nombre del programa es: %s \n", argv[0]);
    printf("Los argumentos del programa son: ");
    for (i=1; i<argc; i++) {
      printf("%s ", argv[i]);
    }
    printf("\n");
    return 0;
  }
#+end_src
  
Hay que poner atención en que al imprimir el nombre del programa el argumento de ~printf~ es ~argv[0]~, tal y como se dijo en el párrafo anterior. Los otros argumentos son impresos por medio del ciclo ~for~, ayudado por ~argc~ para establecer el límite del ciclo. Algunos ejemplos de salida de este programa se colocan a continuación:
  
#+begin_src shell  
  $ ./argumentos alfa beta
  El número de argumentos: 3 
  El nombre del programa es: ./argumentos 
  Los argumentos del programa son: alfa beta 
  $ ./argumentos
  El número de argumentos: 1 
  El nombre del programa es: ./argumentos 
  Los argumentos del programa son:
#+end_src
  
El siguiente código brinda una correspondencia visual de lo explicado anteriormente:

#+begin_src c
  #include <stdio.h>
  
  int main(int argc, char *argv[])
  {
    int i;
    printf("Argumento \t Valor \n");
    for (i=0; i<argc; i++) {
      printf("argv[%i] \t %s \n",i, argv[i]);
    }
    printf("\n");
    return 0;
  }
#+end_src

  Su salida sería:
  
#+begin_src shell
  $ ./argumentos uno dos
  Argumento        Valor 
  argv[0]          ./argumentos 
  argv[1]          uno 
  argv[2]          dos
#+end_src

** Analizar los argumentos

En lenguaje C existe la función ~getopt()~ que permite el análisis básico de los argumentos de un programa. Las variables involucradas en esta función son:

- int *opterr*: Indica si hay un error y lo imprime en la pantalla. Si el valor de la variable es diferente de cero no se imprimirá ningún mensaje.
- int *optopt*: Si se introduce una opción desconocida o incompleta, el número de argumento se almacenará en esta variable.
- int *optind*: Una vez que ~getopt()~ ha encontrado los argumentos de opción, esta variable se usa para indentificar los argumentos que no son de opciones.
- char *optarg*: Si alguna opción necesita un valor, dicho valor estará disponible por medio de esta variable.

En el siguiente programa se definen dos funciones básicas que simulan un mensaje de ayuda (~help()~) y un proceso de búsqueda (~search(char *svalue)~). La primera condición ~if~ se encarga de cerciorarse si el programa corre con argumentos, si no es así, imprime el mensaje de ayuda y termina el programa.

El ciclo ~while~ lee las opciones. Aquí se puede ver como se llama la función ~getopt()~. Lo que es notable aquí es que se identifican las opciones que serán válidas en el programa, en este caso /h/ y /s/. La opción /s/ es seguida de /:/ porque necesitará un valor adicional.

Finalmente, el último condicional ~if~ verifica que existan argumentos que no sean opciones y los imprime como en el programa anterior. Por supuesto, podría llamar otra función en caso de ser necesario.

#+begin_src c
  #include <stdio.h>
  #include <stdlib.h>
  #include <unistd.h>
  
  int help() {
    puts ("Uso del programa: <opciones> [sección]\n");
    puts ("                  [sección]");
    return 0;
  }
  
  int search(char *svalue) {
    printf ("Se buscará el término '%s' en el programa.\n", svalue);
    return 0;
  }
  
  
  int main(int argc, char *argv[])
  {
    int opt;
    int index;
    
    if (argc < 2)
      {
        help ();
        return -1;
      }
  
    while ((opt = getopt(argc, argv, "hs:")) != -1) 
      switch (opt)
        {
        case 'h':
          help();
          break;
        case 's':
          search (optarg);
          break;
        default:
          help();
        }
  
    if(optind != argc)
      {
        printf("Otras instrucciones proporcionadas: \n");
        printf("Argumento \t Valor\n");
        for (index = optind; index < argc ; index++)
          printf ("argv[%i] \t %s \n", index, argv[index]);       
      }
    
    printf ("\n");
    return 0;
  }
#+end_src

Los ejemplos de salida en este caso son los siguientes:

#+begin_src shell
  $ ./parce 
  Uso del programa: <opciones> [sección]
  
  [sección]
  $ ./parce -h
  Uso del programa: <opciones> [sección]
  
  [sección]
  
  $ ./parce -s "hola mundo"
  Se buscará el término 'hola mundo' en el programa.
  
  $ ./parce "hola mundo"
  Otras instrucciones proporcionadas: 
  Argumento        Valor
  argv[1]          hola mundo 
  
  $ ./parce "hola mundo" bar foo
  Otras instrucciones proporcionadas: 
  Argumento        Valor
  argv[1]          hola mundo 
  argv[2]          bar 
  argv[3]          foo 
#+end_src

** Enlaces relacionados

- [[https://www.gnu.org/software/libc/manual/html_node/Using-Getopt.html][Uso de getopt]]
- [[https://gitlab.com/genomorro/ejemplos][Ver código fuente de los ejemplos]]
