; publish.el

; This file is part of My personal blog

; Copyright (C) 2021, Edgar Uriel Domínguez Espinoza

; My personal blog is free software; you can redistribute it and/or modify it under the terms of
; the GNU General Public License as published  by the Free Software Foundation; either version 2
; of the License, or (at your option) any later version.

; My personal blog is distributed in the hope  that it will be useful, but WITHOUT ANY WARRANTY;
; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
; the GNU General Public License for more details.

; You should have received a copy of the GNU General Public License along with My personal blog;
; if not, see <http://www.gnu.org/licenses/> or write  to the Free Software Foundation, Inc., 51
; Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.

; Comment:

; Youtube idea:
; http://endlessparentheses.com/embedding-youtube-videos-with-org-mode-links.html
; Timestamp format
; https://drimyswinteri.ml/esteblog.html
(require 'package)
(package-initialize)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(package-refresh-contents)
(package-install 'htmlize)

(require 'ox-publish)
(require 'htmlize)

;; (set-language-environment "Spanish")
(setq org-html-htmlize-output-type 'css)
(setq org-html-home/up-format
      "<div id=\"org-div-home-and-up\">
        <a accesskey=\"H\" href=\"%s\"> INICIO </a>
       </div>")
(setq org-html-postamble t)
(setq org-html-postamble-format
      '(("en" "<p class=\"author\">Author: %a (%e)</p>
            <p class=\"date\">Last modification: %C</p>
            <p class=\"creator\">%c</p>")
	("es" "<p class=\"author\">Autor: %a (%e)</p>
            <p class=\"date\">Última modificación: %C</p>
            <p class=\"creator\">%c</p>")))

(setq org-publish-project-alist
      '(("org-blog"
	 :base-directory "./org/"
	 :base-extension "org"
	 :recursive t
	 :publishing-directory "./public/"
	 :publishing-function org-html-publish-to-html
	 :auto-sitemap t
	 :language "es"
	 :sitemap-title ""
	 :sitemap-sort-files anti-chronologically
	 :sitemap-format-entry blog-sitemap-entry
	 :sitemap-style list
	 :htmlized-source t
 	 )
	("static-blog"
	 :base-directory "./org/"
	 :base-extension "css\\|png\\|jpg\\|webmanifest"
	 :recursive t
	 :publishing-directory "./public/"
	 :publishing-function org-publish-attachment)
	("blog" :components ("org-blog" "static-blog"))))

(defvar yt-iframe-format
  ;; You may want to change your width and height.
  (concat "<iframe width=\"854\""
          " height=\"480\""
          " src=\"https://www.youtube.com/embed/%s\""
          " frameborder=\"0\""
          " allowfullscreen>%s</iframe>"))

(org-add-link-type
 "yt"
 (lambda (handle)
   (browse-url
    (concat "https://www.youtube.com/embed/"
            handle)))
 (lambda (path desc backend)
   (cl-case backend
     (html (format yt-iframe-format
                   path (or desc "")))
     (latex (format "\href{%s}{%s}"
                    path (or desc "video"))))))

(defvar this-date-format "%A %d de %B de %Y")
(add-to-list 'org-export-options-alist '(:description "DESCRIPTION" nil nil parse))
(add-to-list 'org-export-options-alist '(:keywords "KEYWORDS" nil nil parse))

(defun blog-sitemap-entry (entry style project)
  "Default format for site map ENTRY, as a string.
ENTRY is a file name.  STYLE is the style of the sitemap.
PROJECT is the current project."
  (cond ((not (directory-name-p entry))
	 (format "*[[file:%s][%s]]*
                 #+HTML: <em class='tag'>%s</em>
                 #+HTML: <p>Publicado el %s</p>
                 #+HTML: <p>%s</p>" 
		 entry
		 (org-publish-find-title entry project)
		 (if (eq (type-of (org-publish-find-property entry :keywords project)) 'symbol)
		     (make-string 0 ?x)
		   (org-publish-find-property entry :keywords project))
		 (format-time-string this-date-format
				     (org-publish-find-date entry project))
		 (if (eq (type-of (org-publish-find-property entry :description project)) 'symbol)
		     (make-string 0 ?x)
		   (substring (format "%s" (org-publish-find-property entry :description project)) 1 -1))
		 ))
	((eq style 'tree)
	 ;; Return only last subdir.
	 (file-name-nondirectory (directory-file-name entry)))
	(t entry)))

(provide 'publish-blog)
