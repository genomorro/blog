#+HTML_LINK_UP:   index.html
#+HTML_HEAD:      <link rel="stylesheet" type="text/css" href="css/base.css"/>
#+HTML_HEAD:      <link rel="stylesheet" type="text/css" href="css/htmlize.css" />
#+HTML_HEAD:      <link rel="apple-touch-icon" sizes="180x180" href="img/apple-touch-icon.png">
#+HTML_HEAD:      <link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
#+HTML_HEAD:      <link rel="icon" type="image/png" sizes="16x16" href="img/favicon-16x16.png">
#+HTML_HEAD:      <link rel="manifest" href="img/site.webmanifest">
